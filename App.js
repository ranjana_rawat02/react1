/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { View, StyleSheet} from 'react-native';
// import Login from './src/pages/Login';
import Signup from './src/pages/Signup';
import Header from './src/pages/Header';
import { Container } from 'native-base';
import AppContainer from './src/pages/Routes'


export default class App extends Component {
    render() {
      return (
            <AppContainer />
        );
    }
  }


  const styles = StyleSheet.create({
    
    container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#3f51b5',
     
    },
   
  });

