

import React, {Component} from 'react';
import {Text, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import Logo from './Logo';

export default class Form extends Component {
    render() {
      return (
        <View style={styles.container}>

            <TextInput style={styles.inputBox}
             placeholder= "Email"
             placeholderTextColor = "#fff"/>

            <TextInput style={styles.inputBox}
             placeholder= "Password"
             placeholderTextColor = "#fff" secureTextEntry={true} />

            <TouchableOpacity  style={styles.button}>
                <Text style={styles.buttonText}>{this.props.type}</Text>
            </TouchableOpacity>

        </View>
      );
    }
  }



  const styles = StyleSheet.create({

      container: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
     
    },

    inputBox: {

        width:300,
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        borderRadius: 10,
        paddingHorizontal: 20,
        marginVertical: 10,
        color: '#fff'
    },

    buttonText : {

        fontSize: 16,
        fontWeight: '400',
        color: '#fff',
        textAlign: 'center'

    },

    button : { 

        width:300,
        marginVertical: 10,
        backgroundColor: '#283593',
        borderRadius: 10,
        paddingVertical: 10

    }
   
  });