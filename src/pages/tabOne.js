import React, { Component } from 'react';
import { View, Text , Image } from 'react-native';
import { Container, Content, Accordion,Footer,FooterTab,Button, } from 'native-base';

 
const dataArray = [
    { title: "Aish", content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." },
    { title: "Sourabh", content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." },
    { title: "Ranjana", content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." }
  ];

class TabOne extends Component {
  


  render() {
    return (
      <Container>
          <Content style={{ flex:1 }}>
              <Accordion dataArray={dataArray} icon="add" expandedIcon="remove"  iconStyle={{ color: "green" }}
            expandedIconStyle={{ color: "red" }}  headerStyle={{ backgroundColor: "#b7daf8" }}
            contentStyle={{ backgroundColor: "#ddecf8" }}/>

           
          </Content>

        <Footer>
          <FooterTab>
            <Button>
              <Text>Apps</Text>
            </Button>
            <Button>
              <Text>Camera</Text>
            </Button>
            <Button active>
              <Text>Navigate</Text>
            </Button>
            <Button>
              <Text>Contact</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default TabOne;
