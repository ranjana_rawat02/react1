import React, { Component } from 'react';
import { View, Text , Image } from 'react-native';
import { Container, Content,Fab,Icon,Button,Footer,FooterTab,Badge,Card,CardItem,Right} from 'native-base';



class TabTwo extends Component {
    constructor(props) {
        super(props)
        this.state = {
          active: 'true'
        };
      }


  render() {
    return (
      <Container>   
      <Content style={{ flex:1,marginHorizontal:10}}>

          <Card>
            <CardItem>
              <Icon active name="logo-googleplus" />
              <Text>Google Plus</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
             </CardItem>
           
           </Card>
          <Card>
            <CardItem>
            
              <Icon active name="logo-reddit" />
              <Text>Reddit</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
             </CardItem>
           
           </Card>
          <Card>
            <CardItem>
              <Icon active name="logo-googleplus" />
              <Text>Google Plus</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
             </CardItem>
           
           </Card>

      </Content>

       



      <Footer>
          <FooterTab>
            <Button badge vertical>
              <Badge><Text>2</Text></Badge>
              <Icon name="apps" />
              <Text>Apps</Text>
            </Button>
            <Button vertical>
              <Icon name="camera" />
              <Text>Camera</Text>
            </Button>
            <Button active badge vertical>
              <Badge ><Text>51</Text></Badge>
              <Icon active name="navigate" />
              <Text>Navigate</Text>
            </Button>
            <Button vertical>
              <Icon name="person" />
              <Text>Contact</Text>
            </Button>
          </FooterTab>
        </Footer>
     
     
      </Container>
    );
  }
}

export default TabTwo;