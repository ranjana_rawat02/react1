0
import React, { Component } from 'react';
import { View, Text , StyleSheet , StatusBar } from 'react-native';
import Login from '../pages/Login';

class App extends Component{
    render() {
      return (
        <View style={styles.container}>
          <StatusBar backgroundColor = "#9fa8da" barStyle= "light-content"/>
          <Login/>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#3f51b5',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      color:'white',
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
  


export default App;


