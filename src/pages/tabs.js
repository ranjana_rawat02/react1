import React, { Component } from 'react';
import { Container,Tab, Tabs,TabHeading,Icon} from 'native-base';
import {Text} from 'react-native';
import Tab1 from './tabOne';
import Tab2 from './tabTwo';
import Tab3 from './tabThree';

export default class TabsExample extends Component {
  render() {
    return (
      <Container>
        <Tabs>
          <Tab heading={ <TabHeading><Icon name="camera" style={{ fontSize:15}}/><Text style={{ color:'white',marginLeft:7}}>Camera</Text></TabHeading>}>
            <Tab1 />
          </Tab>
          <Tab heading="Tab2">
            <Tab2 />
          </Tab>     
          <Tab heading="Tab3">
            <Tab3 />
          </Tab>     
        </Tabs>
      </Container>
    );
  }
}