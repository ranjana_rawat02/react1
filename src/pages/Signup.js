

import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
// import Logo from '../component/Logo';
// import Form from '../component/Form';
import { Body ,Header, Container,Left,Right,Icon} from 'native-base';



export default class Signup extends Component {
    render() {
      return (
        <Container>
            <Header style={styles.header}>
            <Left>
            <Icon name='chevron-left' type='MaterialIcons' style={{ color:'white' }}/>
            </Left>
            <Body><Text style={styles.headerText}>MYAPP</Text></Body>
            <Right><Icon name='home' style={{ color:'white' }}/></Right>
            </Header>
            
            <View style={{ flexDirection:'row',flex:0.5}}>
           <TouchableOpacity onPress={()=>this.getTabpages()} style={{ alignItems:'center',backgroundColor:'red',justifyContent:'center',flex:0.5,flexDirection:'row'}}> 
           
            
                  <Text>Ranjana</Text>
                  <Text>Sourabh</Text>
                  <Text>Aish</Text>
            
            
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.getSignUpPage()} style={{ alignItems:'center',backgroundColor:'blue',justifyContent:'center',flex:0.5,flexDirection:'row'}}> 
           
            
                  <Text>Register</Text>
                  
            
            
              </TouchableOpacity>
            </View>      
        </Container>  
      ); 
    }

    getTabpages = () => {
     
        this.props.navigation.navigate('TabsDemo');
}
  getSignUpPage = () => {
     
        this.props.navigation.navigate('Register');
}

}
 


  const styles = StyleSheet.create({
    header: {
  
      backgroundColor: '#558b2f',
     
     
    },
    headerText:{
      color: '#ffffff',
    },

    signupTextCont: {

      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
      marginVertical: 16,
      flexDirection: 'row',

    },

    signupText: {

      color: 'rgba(255, 255, 255, 0.5)',
      fontSize: 16

    },

    signupButton: {

        color: '#ffffff',
        fontSize: 16,
        fontWeight: '400'
    }

   
  });