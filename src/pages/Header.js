import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Text, Icon} from 'native-base';

export default class HeaderComponent extends Component 
{
  render() {
    return (
        <Header>
          <Left>
            <Icon name={'chevron-left'} type={'FontAwesome'} style={{ color:'#fff' }}/>
          </Left>
          <Body style={{ alignItems:'center', justifyContent:'center' }}>
            <Text style={{ color:'#fff' }}>Header</Text>
          </Body>
          <Right />
        </Header>
      
    );
  }
}