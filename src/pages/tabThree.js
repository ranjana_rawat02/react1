import React, { Component } from 'react';
import { Container, Content, DatePicker, Text, Button } from 'native-base';
// import { HeaderStyleInterpolator } from 'react-navigation';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from 'react-native-responsive-dimensions'
import { StyleSheet } from 'react-native'
export default class DatePickerExample extends Component {
  constructor(props) {
    super(props);
    this.state = { 
        chosenDate: new Date(),
        text:'Todays Date',
     };
     this.setDate = this.setDate.bind(this);
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }
  render() {
    return ( 
      <Container>
        <Content>
        

          <DatePicker
            defaultDate={new Date(2018, 4, 4)}
            minimumDate={new Date(1994, 1, 1)}
            maximumDate={new Date(2018, 12, 31)}
            locale={"en"}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={"fase"}
            androidMode={"default"}
            placeHolderText="Select date"
            textStyle={{ color: "blue" }}
            placeHolderTextStyle={styles.pickerBtn}
           
            disabled={false}
            />
            <Text>
             {this.state.text}: {this.state.chosenDate.toString().substr(4, 12)}
            </Text>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    pickerBtn:{ 
        width:responsiveWidth(40),
        height:responsiveHeight(8),
        alignSelf: 'center',
        textAlign:'center',
        borderRadius:10,
        backgroundColor:'blue',
        color:'#ffffff'
     }
})

