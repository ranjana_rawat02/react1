import React, { Component } from 'react';

import {createAppContainer,createStackNavigator} from 'react-navigation';
import SplashScreen from './splash';
import Signup from './Signup';
import Register from './Register';
import Signin from './Signin';
import TabsDemo from './tabs';


const AppContainer = createStackNavigator({
  
    SplashScreen:SplashScreen,
    Signup:Signup,
    TabsDemo:TabsDemo,
    Register:Register,
    Signin:Signin
},
{
    initialRouteName:'SplashScreen',
    headerMode: 'none',
    navigationOptions: {
    headerVisible: false,
  }
    
});


export default createAppContainer(AppContainer);