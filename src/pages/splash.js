import React, { Component } from 'react';
import { View, Text , Image } from 'react-native';
import { Container, Content } from 'native-base';
const img = require('../images/logo.png')


class SplashScreen extends Component {
   


  componentDidMount(){
      setTimeout(()=>{
        this.props.navigation.navigate('Signup');
       
      },1500)
      
  }

  render() {
    return (
      <Container>
          <Content contentContainerStyle={{ justifyContent:'center', 
                                            backgroundColor:'#039be5'  , 
                                            alignItems:'center',
                                            flex:1}} >
                <Image source={img} style={{ width:80,height:80 }} resizeMode={'contain'} />
          </Content>
      </Container>
    );
  }
}

export default SplashScreen;
