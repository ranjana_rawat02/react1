import React, { Component } from 'react';
import { View, Text,StyleSheet,Row} from 'react-native';
import { Switch,Container,Body,Header,Left,Icon,Button,Title, Content, Form,Input,Item,Label, Footer, FooterTab} from 'native-base';


class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:''
    };
  }

  render() {
    return (
     
        <Container>
        <Header style={styles.headerStyle}>
          <Left>
            <Button transparent>
              <Icon style={styles.iconStyle} name='chevron-left' type='MaterialIcons' />
            </Button>
          </Left>
          <Body>
            <Title style={styles.iconStyle} >SIGNUP</Title>
          </Body>
        </Header>
        <Content>
          <Form style={styles.formStyle}>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <Item style={styles.inputStyle} floatingLabel>
              <Label style={styles.labelText}>First Name</Label>
              <Input onChangeText={(text)=> this.setState({name:text})} value={this.state.name}/>
            </Item> 
            <Item style={styles.inputStyle} floatingLabel>
              <Label style={styles.labelText}>Last Name</Label> 
              <Input />
            </Item>
          </View>

            <Item style={styles.inputStyle} floatingLabel>
              <Label style={styles.labelText}>Password</Label>
              <Input />
            </Item>
            <Item  style={styles.inputStyle} floatingLabel>
              <Label style={styles.labelText}>Retype Password</Label>
              <Input />
            </Item>
            <View style={{flex: 1, flexDirection: 'row'}} >
              <Item style={{flex:0.5}} floatingLabel>
                <Label style={styles.labelText} >DD</Label>
                <Input onChangeText={(text)=> this.setState({name:text})} value={this.state.name}/>
              </Item> 
              <Item  style={{flex:0.5}} floatingLabel>
                <Label style={styles.labelText}>MM</Label> 
                <Input />
              </Item>
              <Item style={{flex:0.5}}  floatingLabel>
                <Label style={styles.labelText}>YYYY</Label> 
                <Input />
              </Item>
           </View>
            <Item  style={styles.inputStyle} floatingLabel>
              <Label style={styles.genderText}>SELECT YOUR GENDER</Label>
              <Input />
            </Item>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={[styles.smallText,{flex:0.5}]}> TERMS AND CONDITIONS</Text>
              <Switch value={false}  style={{flex:0.5,marginTop:30}}/>
            </View>

            <Button style={styles.signupButton} block warning onPress={()=> this.submitBtn()}>
              <Text>SIGNUP</Text>
            </Button>
            
          </Form>
         
        </Content>
         <Footer style={styles.headerStyle}>
         <Text>Already a member ? </Text>
         <Text style={{color:'#ff9800'}} onPress={()=>this.signIn()}> SIGNIN </Text>
            </Footer>
        </Container>
    );
  }

  submitBtn(){
    // Add a new document with a generated id.
db.collection("cities").add({
  name: this.state.name,
  email: this.state.email,
  password: this.state.password,
  phone: this.state.phone
})
.then(function(docRef) {
  console.log("Document written with ID: ", docRef.id);
})
.catch(function(error) {
  console.error("Error adding document: ", error);
});
  }

  signIn= () => {
    this.props.navigation.navigate('Signin');
  }
}
const styles = StyleSheet.create({
  headerStyle:{
    backgroundColor:'#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center',
    
  },

  iconStyle:{
      color:'black', 
     
  },
  inputStyle:{
     marginTop:15,
      paddingBottom:5,
      flex:0.5,   
  },
  labelText:{
    fontSize:15
  },
  formStyle:{
      marginRight:10,   
  },
  smallText:{
      marginTop:30,   
      color:'#ffb74d',
      marginLeft:10,
      marginBottom:0
  },
  genderText:{ 
      color:'#ffb74d',
      marginBottom:0,
      fontSize:14
  },
  signupButton:{
    marginLeft:10,
    marginTop:35,
    borderColor: '#ddd',
    elevation:3,
  }

})

export default Register;
