import React, { Component } from 'react';
import { View, Text,StyleSheet,Row} from 'react-native';
import { Switch,Container,Body,Header,Left,Icon,Button,Title, Content, Form,Input,Item,Label, Footer, FooterTab} from 'native-base';


class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:''
    };
  }

  render() {
    return (
     
        <Container>
        <Header style={styles.headerStyle}>
          <Left>
            <Button transparent>
              <Icon style={styles.iconStyle} name='chevron-left' type='MaterialIcons' />
            </Button>
          </Left>
          <Body>
            <Title style={styles.iconStyle} >SIGNUP</Title>
          </Body>
        </Header>
        
            <View style={{flex:0.5}}>
                <Form style={styles.formStyle}>
                <Item style={styles.inputStyle} >
                        <Icon active name='mail' />
                        <Input placeholder='Email'/>
                    </Item>
                    <Item style={styles.inputStyle} >
                        <Icon active name='lock' type='MaterialIcons' />
                        <Input placeholder='Password'/>
                    </Item>
                    <Button style={styles.signupButton} block warning   onPress={()=> this.submitBtn()}>
                        <Text style={{color:'white'}}>SIGN IN</Text>
                    </Button>
                </Form>

               
            </View>
            <View style={{backgroundColor:'red',flex:0.5}}>
                <Form>
                  
                </Form>
            </View>
         

        </Container>
    );
  }


  signUp= () => {
    this.props.navigation.navigate('Register');
  }
}
const styles = StyleSheet.create({
  headerStyle:{
    backgroundColor:'#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconStyle:{
      color:"black"
  },
  inputStyle:{
    marginTop:15,
    paddingBottom:5,
    borderWidth: 0
  },
  signupButton:{
    marginLeft:10,
    marginTop:35,
    borderColor: '#ddd',
    elevation:3,
  },
  formStyle:{
    marginRight:15,   
}
})

export default Signin;
