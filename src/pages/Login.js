

import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Logo from '../component/Logo';
import Form from '../component/Form';

export default class Login extends Component {
    render() {
      return (
        <View style={styles.container}>
          <Logo/>
          <Form type="Login"/>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText} >Dont't have an account yet?fdfggfhh </Text>
            <Text style={styles.signupButton}>Signup</Text>
          </View>
        </View>
      );
    }
  }



  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#3f51b5',
     
    },

    signupTextCont: {

      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
      marginVertical: 16,
      flexDirection: 'row',

    },

    signupText: {

      color: 'rgba(255, 255, 255, 0.5)',
      fontSize: 16

    },

    signupButton: {

        color: '#ffffff',
        fontSize: 16,
        fontWeight: '400'
    }

   
  });